package chatty;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class CustomInvocationHandler implements InvocationHandler {

    private final int CACHE_SIZE = 1;
    private static Logger LOGGER = LoggerFactory.getLogger(CustomInvocationHandler.class);
    private final Map<String, Method> methods = new HashMap<>();

    private Object target;

    private Gson gson = new Gson();

    private List<InvocationData> invocationRecords;

    CustomInvocationHandler(Object target) {
        this.target = target;
        invocationRecords = new ArrayList<>();

        for (Method method : target.getClass().getDeclaredMethods()) {
            this.methods.put(method.getName(), method);
        }
        LOGGER.debug(String.valueOf(this.methods.size()));
    }

    // Invocation wrapper. We record input args, output result and invocation time.
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long start = System.nanoTime();
        Object result = methods.get(method.getName()).invoke(target, args);
        long elapsed = System.nanoTime() - start;

        writeInvocationRecords(new InvocationData(
                method.getName(),
                args,
                result,
                elapsed
        ));

        LOGGER.info("Executing {} finished in {} ns", method.getName(), elapsed);

        return result;
    }


    // We save invocation record to array and then write it to file lazily.
    private void writeInvocationRecords(InvocationData invocationData) {
        invocationRecords.add(invocationData);
        if (invocationRecords.size() >= CACHE_SIZE) {
            try {
                List<InvocationData> tempRecords = invocationRecords;
                FileWriter myWriter = new FileWriter("invocation_records.json", true);
                invocationRecords.clear();

                tempRecords.forEach(record -> {
                    String jsonString = gson.toJson(record);
                    try {
                        myWriter.write(jsonString + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                myWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
