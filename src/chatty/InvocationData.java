package chatty;

public class InvocationData {
    private String methodName;
    private Object[] inputArgs;
    private Object returnValue;
    private long invocationTime;

    public InvocationData(String methodName, Object[] inputArgs, Object returnValue, long    invocationTime) {
        this.methodName = methodName;
        this.inputArgs = inputArgs;
        this.returnValue = returnValue;
        this.invocationTime = invocationTime;
    }

}
