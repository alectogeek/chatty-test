package chatty;

import java.lang.reflect.Proxy;


// This is a custom class loader.
// I created it to be able to wrap original class with proxy and use functions/methods invocation handler
//

public class CustomClassLoader extends ClassLoader {

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        System.out.println("Class Loading Started for " + name);

        if (name.equals("chatty.TwitchClient")) {
            final Class loadedClass = super.loadClass(name);
            CustomInvocationHandler customInvocationHandler = new CustomInvocationHandler(loadedClass);
            return Proxy.newProxyInstance(loadedClass.getClassLoader(), new Class[]{loadedClass}, customInvocationHandler).getClass();
        }
        return super.loadClass(name);
    }

}

